<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata(){
        return view('register');
    }
    public function come(request $request){
        $namef = $request['namafir'];
        $namel = $request['namalast'];
        return view('welcome', compact('namef','namel'));
    }
}
